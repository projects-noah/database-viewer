import { Controller, Get, Header, HttpCode } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
	constructor(private readonly appService: AppService) {}

	@Get()
	@HttpCode(200)
	@Header('Content-Type', 'application/json')
	getHello(): { name: string } {
		return { name: this.appService.getHello() };
	}
}
