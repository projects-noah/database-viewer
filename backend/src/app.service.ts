import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
	getHello(): string {
		console.log('Hello from backend');
		return 'It is working with backend!';
	}
}
