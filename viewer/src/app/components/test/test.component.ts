import { Component, OnInit } from '@angular/core';
import { TestService } from '../../services/test.service';

@Component({
  selector: 'app-test',
  templateUrl: './test.component.html',
  styleUrls: ['./test.component.css']
})
export class TestComponent implements OnInit {
	display: string = "Not loaded yet";

  	constructor(private testService: TestService) { }

	ngOnInit(): void {
		this.testService.getTest().subscribe((data) => {
			this.display = data.name;
		});
  	}

}
