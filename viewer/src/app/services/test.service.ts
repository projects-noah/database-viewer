import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

const httpOptions = {
	headers: new HttpHeaders({
		'Content-Type': 'application/json',
	}),
};

@Injectable({
  providedIn: 'root'
})
export class TestService {
	// Make this backend call to an external API
	// Use localhost:3000 for local testing or demos.
	private testUrl = 'api/';

	constructor(private http: HttpClient) { }

	getTest() {
		return this.http.get<Test>(this.testUrl);
	}
}

type Test = {
	name: string;
};
