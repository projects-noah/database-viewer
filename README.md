# Database-Viewer

[![build](https://img.shields.io/gitlab/pipeline-status/projects-noah/database-viewer?branch=main&style=for-the-badge)](https://gitlab.com/projects-noah/database-viewer/-/pipelines)
[![covarage](https://img.shields.io/gitlab/coverage/projects-noah/database-viewer/main?style=for-the-badge)](https://gitlab.com/projects-noah/database-viewer)

## Table of Contents

1. [Description](#description)
   1. [Versioning](#versioning)
   2. [Releases](#releases)
2. [Getting started](#getting-started)
   1. [Visuals](#visuals)
   2. [Prerequisites](#prerequisites)
   3. [Dependencies](#dependencies)
3. [Usage](#usage)
   1. [Installation](#installation)
   2. [Running](#running)
4. [Support](#support)
5. [Contributing](#contributing)
6. [Authors and acknowledgment](#authors-and-acknowledgment)
7. [License](#license)
8. [Project status](#project-status)
   1. [Roadmap](#roadmap)
   2. [Release history](#release-history)

## Description

This is a database viewer for relational databases written with the [Angular](https://angular.io/) framework. It can be run as a stand alone docker container or directly with the docker-compose file. The advantage of running it with the docker-compose file is that it directly instantiates the database within a separate container and connects with it.

If you do not want to use docker, you can also run the database viewer as a standalone application for this read the instructions under the [usage](#usage) section.

### Versioning

This viewer is versioned as ` major.minor.patch ` from the [semver](https://semver.org/) versioning standard. Next to that it is possible it has a suffix in the next formatting:

| Stage                   | Version       |
| ------                  |:-------------:|
| **Alpha**               | 1.1.0-a.1     |
| **Beta**                | 1.1.0-b.4     |
| **Release candidate**   | 1.1.0-rc.2    |
| **Release**             | 1.2.0         |
| **Post-release fixes**  | 1.2.4         |

### Releases

[![Release version](https://img.shields.io/gitlab/v/release/projects-noah/database-viewer?sort=semver&style=for-the-badge)](https://gitlab.com/projects-noah/database-viewer/-/releases)

[![Pre-release version](https://img.shields.io/gitlab/v/release/projects-noah/database-viewer?include_prereleases&sort=semver&style=for-the-badge)](https://gitlab.com/projects-noah/database-viewer/-/releases)

## Getting started

Every project is different, so consider which of these sections apply to yours. The sections used in the template are suggestions for most open source projects. Also keep in mind that while a README can be too long and detailed, too long is better than too short. If you think your README is too long, consider utilizing another form of documentation rather than cutting out information.

### Visuals

Depending on what you are making, it can be a good idea to include screenshots or even a video (you'll frequently see GIFs rather than actual videos). Tools like ttygif can help, but check out Asciinema for a more sophisticated method.

### Dependencies

You need to follow the steps below to install the dependencies.

1. If you are using the docker image:
   - Install [Docker](https://www.docker.com/get-started)
   - Install [PostgreSQL](https://www.postgresql.org/download/).
   - Make sure `postgresql` is running and fully working.
2. If you are using the docker-compose file:
   - Install [Docker](https://www.docker.com/get-started)
   - Install [Docker Compose](https://docs.docker.com/compose/install/)
3. If you are using the standalone application:
   - Install [nodejs](https://nodejs.org/en/download/).
   - Make sure `nodejs` is at least version `16.0.0` and working correctly.
   - Install [PostgreSQL](https://www.postgresql.org/download/).
   - Make sure `postgresql` is running and fully working.

## Usage

Use examples liberally, and show the expected output if you can. It's helpful to have inline the smallest example of usage that you can demonstrate, while providing links to more sophisticated examples if they are too long to reasonably include in the README.

### Installation

If you are using `docker-compose` there is nothing extra you need to install, as all will be done within the docker image.
While if you run the `docker` yourself you need to install and run at least a `postgresql` database in either a separate container or directly on the host.

If you run natively, you need to follow the following steps to install the `viewer`:

1. Clone the repository or download the source code. `git clone git@gitlab.com:projects-noah/database-viewer.git`
2. Run `cd viewer` to move into the directory.
3. Run `npm install` to install the dependencies.

With the docker image you can execute the following commands to start the database viewer:

1. Get the docker image
   1. Download the image:
      - `Download command`
   2. Build the docker image from source:
      - Clone the repository or download the source code. `git clone git@gitlab.com:projects-noah/database-viewer.git`
      - `docker build -t IMAGE_NAME:IMAGE_TAG .`

### Running

There are three ways to run the database viewer: [`native/standalone`](#native/standalone), [`docker`](#docker) and [`docker-compose`](#docker-compose). With the native and docker solutions only being viable if you already have a `postgresql` database running.

#### Native/standalone

1. Once you have followed the installation instructions correctly, you can start this process.
2. Make sure your `postgresql` database is running.
3. Go into the viewer directory, if you are not already there, by running `cd /path/to/gitrepoclone/viewer`.
4. To start with the development environment go to step 5. To start with the production environment go to step 9.
5. Run `npm run watch` to start the building of the development environment.
6. Open a new terminal with the same directory.
7. Start the application by running `npm start`.
8. Go to localhost:4200 in your browser to see the application.
9. Install a webserver, either [Apache](https://httpd.apache.org/) or [Nginx](https://nginx.org/) or any other server that can display html files, and start the process.
10. Run `npm run build` to start the building of the production environment.
11. Copy the contents of the `dist/viewer` directory to the root of your webserver, as these are html files with some javascript and css files.

#### Docker

1. Start the database viewer with the created/downloaded docker image:
   1. Run the docker image:
      - `docker run -p HOST_PORT:80 --name CONTAINER_NAME IMAGE_NAME`
   2. Run the docker image in detached mode:
      - `docker run -d -p HOST_PORT:80 --name CONTAINER_NAME IMAGE_NAME`

#### Docker-compose

1. Go to the directory where the docker-compose.yml file is located. The root of the cloned repository.
2. Run the command `docker-compose up -d` to start the database viewer.

## Support

<!-- Tell people where they can go to for help. It can be any combination of an issue tracker, a chat room, an email address, etc. -->
There is no support sutep yet for this project.

## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (` git checkout -b feature/AmazingFeature `)
3. Commit your Changes (` git commit -m 'AmazingFeature: message on what has changed' `) Note: The message should fit in the sentence `This commit will` and then your message. Example `This commit will` `message on what has changed`.
4. Push to the Branch (` git push origin feature/AmazingFeature `)
5. Open a Pull Request

## Authors and acknowledgment

A great thanks to all the people who have helped make this project a reality.

- Noah Knegt: [@Westlanderz](https://gitlab.com/Westlanderz)

## License

Distributed under the ` MIT License `. See `LICENSE` for more information

## Project status

This project is in **active development**. The current stable version is `0.1.0` and if you want to compare the different versions, you can read the [release history](#release-history) below. If you want to check up and coming features there is a roadmap available [here](#roadmap).

### Roadmap

Nothing on the ROADMAP at this given moment, if you would like a feature to be added either fill in a feature request [here](https://gitlab.com/projects-noah/database-viewer/-/issues) or follow the steps in the [contributing](#contributing) section.

### Release history

No releases yet.
